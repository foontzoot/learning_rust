pub fn number_prompt(counter: i64) -> i64 {
    println!("Enter number {counter}:");
    let mut return_val: i64 = 0;
    let mut buffer = String::new();
    std::io::stdin()
        .read_line(&mut buffer)
        .expect("failed to read from stdin");

    let trimmed_buffer = buffer.trim();
    match trimmed_buffer.parse::<i64>() {
        Ok(i) => {
            return_val = i;
        }
        Err(..) => println!("Not a number: {}", trimmed_buffer),
    };

    return return_val;
}
