use std::io;

mod add;
mod number_prompt;

fn main() {
    let mut first_num: i32 = 0;
    let mut second_num: i32 = 0;

    println!("Enter first digit:");
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("failed to read from stdin");

    let trimmed_buffer = buffer.trim();
    match trimmed_buffer.parse::<i32>() {
        Ok(i) => {
            first_num = i;
        }
        Err(..) => println!("this was not an integer: {}", trimmed_buffer),
    };

    println!("Enter second digit:");
    let mut buffer = String::new();
    io::stdin()
        .read_line(&mut buffer)
        .expect("failed to read from stdin");

    let trimmed_buffer = buffer.trim();
    match trimmed_buffer.parse::<i32>() {
        Ok(i) => {
            second_num = i;
        }
        Err(..) => {
            println!("this was not an integer: {}", trimmed_buffer)
        }
    };

    let result = add::math_add(first_num, second_num);
    println!(
        "Adding {} and {} resulted in {}",
        first_num, second_num, result
    );
}
